type UpdatableStateKey = "color" | "mode"

export type State = {
  color: string
  mode: "write" | "erase"
  update: <K extends UpdatableStateKey>(key: K, value: State[K]) => void
}

export function newState(): State {
  return {
    color: "currentcolor",
    mode: "write",
    update(key, value) {
      this[key] = value
    },
  }
}
