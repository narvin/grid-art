import "./style.css"
import * as colorPicker from "./colorPicker"
import * as grid from "./grid"
import * as state from "./state"

const appState = state.newState()
grid.insertGrid({ appState, width: 60, height: 60, size: 12 })
colorPicker.insertColorPicker({
  appState,
  colors: [
    "#FF90BC",
    "#FFC0D9",
    "#F9F9E0",
    "#8ACDD7",
    "#711DB0",
    "#C21292",
    "#EF4040",
    "#FFA732",
    "#FFECD6",
    "#4CB9E7",
    "#3559E0",
    "#0F2167",
  ],
})
