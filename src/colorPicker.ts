import * as state from "./state"

export type ColorPickerProps = {
  appState: state.State
  colors: string[]
}

type SelectSwatchByElement = (swatch: HTMLDivElement) => void
type SelectSwatchByIndex = (idx: number) => void
export type SwatchSelectors = {
  selectSwatchByElement: SelectSwatchByElement
  selectSwatchByIndex: SelectSwatchByIndex
}

function createSwatchSelectors(appState: state.State): SwatchSelectors {
  function selectSwatchByElement(swatch: HTMLDivElement) {
    const swatches = document.querySelectorAll(".color-picker__swatch")
    swatches.forEach(function removeClass(s) {
      s.classList.remove("color-picker__swatch--active")
    })
    swatch.classList.add("color-picker__swatch--active")
    appState.update("color", swatch.style.backgroundColor)
  }
  function selectSwatchByIndex(idx: number) {
    const swatches = document.querySelectorAll<HTMLDivElement>(
      ".color-picker__swatch",
    )
    if (idx < swatches.length) {
      const swatch = swatches[idx]
      selectSwatchByElement(swatch)
    }
  }
  return { selectSwatchByElement, selectSwatchByIndex }
}

function createSwatchOnClickHandler(swatchSelector: SelectSwatchByElement) {
  return function pickSwatchOnClick(e: MouseEvent) {
    const { target } = e
    if (target instanceof HTMLDivElement) {
      swatchSelector(target)
    }
  }
}

function createSwatch(picker: HTMLDivElement, color: string) {
  const swatch = document.createElement("div")
  swatch.style.setProperty("background-color", color)
  swatch.classList.add("color-picker__swatch")
  picker.append(swatch)
}

export function insertColorPicker({ appState, colors }: ColorPickerProps) {
  const app = document.querySelector("#app")!
  const picker = document.createElement("div")
  const { selectSwatchByElement, selectSwatchByIndex } =
    createSwatchSelectors(appState)
  picker.classList.add("color-picker")
  picker.addEventListener(
    "click",
    createSwatchOnClickHandler(selectSwatchByElement),
  )
  colors.forEach((color) => createSwatch(picker, color))
  app.append(picker)
  selectSwatchByIndex(0)
  return { selectSwatchByElement, selectSwatchByIndex }
}
