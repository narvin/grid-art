import * as state from "./state"

const CANVAS_COLOR = "#d3d3d3"

type MouseEventHandler = (e: MouseEvent) => void
type KeyboardEventHandler = (e: KeyboardEvent) => void

function setPixelColor(appState: state.State, pixel: HTMLDivElement) {
  const { color, mode } = appState
  pixel.style.setProperty(
    "background-color",
    mode === "write" ? color : CANVAS_COLOR,
  )
}

function createPixelOnClickHandler(appState: state.State): MouseEventHandler {
  return function handler(e: MouseEvent) {
    const { target } = e
    if (target instanceof HTMLDivElement) {
      setPixelColor(appState, target)
    }
  }
}

function createPixelOnMoveHandler(appState: state.State): MouseEventHandler {
  return function handler(e: MouseEvent) {
    const { buttons, target } = e
    if (target instanceof HTMLDivElement && buttons === 1) {
      setPixelColor(appState, target)
    }
  }
}

function createOnKeyPressHandler(appState: state.State): KeyboardEventHandler {
  return function setModeOnPress(e: KeyboardEvent) {
    if (e.key === "Shift") {
      appState.update("mode", "erase")
    }
  }
}

function createOnKeyUpHandler(appState: state.State): KeyboardEventHandler {
  return function setModeOnPress(e: KeyboardEvent) {
    if (e.key === "Shift") {
      appState.update("mode", "write")
    }
  }
}

function createPixel(grid: HTMLDivElement) {
  const pixel = document.createElement("div")
  pixel.className = "grid__pixel"
  grid.append(pixel)
}

export type GridProps = {
  appState: state.State
  width: number
  height: number
  size: number
}

export function insertGrid({ appState, width, height, size }: GridProps) {
  const app = document.querySelector("#app")!
  const grid = document.createElement("div")
  grid.classList.add("grid")
  grid.tabIndex = 0
  grid.addEventListener("click", createPixelOnClickHandler(appState))
  grid.addEventListener("mouseenter", createPixelOnMoveHandler(appState), {
    capture: true,
  })
  grid.addEventListener("keydown", createOnKeyPressHandler(appState))
  grid.addEventListener("keyup", createOnKeyUpHandler(appState))
  Array.from({ length: width * height }, (_) => createPixel(grid))
  document.documentElement.style.setProperty("--grid-width", width.toString())
  document.documentElement.style.setProperty("--grid-size", `${size}px`)
  app.append(grid)
}
